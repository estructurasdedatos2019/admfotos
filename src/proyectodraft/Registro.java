/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodraft;

import Funcionamiento.Album;
import Funcionamiento.Foto;
import Funcionamiento.Usuario;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author Karla
 */
public class Registro {
    private static LinkedList<Usuario> usuariosRegistro = new LinkedList<>();
    private static HashMap<String, ArrayList<ArrayList<Object>>> mapEtiquetasGlobalesFoto = new HashMap<>(); 
    private static HashMap<String, ArrayList<ArrayList<Object>>> mapPersonasGlobalesFoto = new HashMap<>();
    
    
    public static void guardarUsuarios(){   //Serializa la lista de usuarios
        try{
            FileOutputStream out = new FileOutputStream("usuarios.ser");
            ObjectOutputStream fileOut = new ObjectOutputStream(out);
            fileOut.writeObject(usuariosRegistro);
            fileOut.close();
            out.close();
        }catch(IOException e){
            System.out.println("No se pudieron guardar los usuarios.");
        }
    }
    
    public static void llenarMapEtiquetasGlobalesFoto(Foto foto, Usuario user){
        for(String etiqueta: foto.getEtiquetas()){
            if(mapEtiquetasGlobalesFoto.keySet().contains(etiqueta)){
                ArrayList<Object> listaPorFoto = new ArrayList<>();
                listaPorFoto.add(user);
                listaPorFoto.add(foto);
                mapEtiquetasGlobalesFoto.get(etiqueta).add(listaPorFoto);
            }
            else{
                ArrayList<ArrayList<Object>> valor = new ArrayList<>();
                ArrayList<Object> listaPorFoto = new ArrayList<>(); // SIEMPRE PRIMERO SE LO AÑADE AL USER, LUEGO LA FOTO
                listaPorFoto.add(user);
                listaPorFoto.add(foto);
                valor.add(listaPorFoto);
                mapEtiquetasGlobalesFoto.put(etiqueta, valor);
            }
        }
    }
    public static void llenarMapPersonasGlobalesFoto(Foto foto, Usuario user){ // La lista de personas de foto debería ser ArrayList<Usuario>
        for(String persona: foto.getPersonasEnFoto()){
            if(mapPersonasGlobalesFoto.keySet().contains(persona)){
                ArrayList<Object> listaPorFoto = new ArrayList<>();
                listaPorFoto.add(user);
                listaPorFoto.add(foto);
                mapPersonasGlobalesFoto.get(persona).add(listaPorFoto);
            }
            else{
                ArrayList<ArrayList<Object>> valor = new ArrayList<>();
                ArrayList<Object> listaPorFoto = new ArrayList<>();
                listaPorFoto.add(user);
                listaPorFoto.add(foto);
                valor.add(listaPorFoto);
                mapEtiquetasGlobalesFoto.put(persona, valor);
            }
        }
    }
    
    public static void cargarUsuarios(){ //Deserializa la lista de Usuarios y la devuelve
        try{
            FileInputStream in = new FileInputStream("usuarios.ser");
            ObjectInputStream FileIn = new ObjectInputStream(in);
            usuariosRegistro = (LinkedList<Usuario>)FileIn.readObject();
            FileIn.close();
            in.close();
        } catch(IOException e){
            System.out.println("No el archivo no existe");
        } catch(ClassNotFoundException e){
            System.out.println("Clase no encontrada");
        }
    }
    public static LinkedList<Usuario> getUsuariosRegistro(){
        return Registro.usuariosRegistro;
    }

}
