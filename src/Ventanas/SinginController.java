/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Usuario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import proyectodraft.Registro;

/**
 * FXML Controller class
 *
 * @author Karla
 */
public class SinginController implements Initializable {

    @FXML Label nombreErrorLabel;
    @FXML TextField nombresTField;
    @FXML Label apellidosErrorLabel;
    @FXML TextField apellidosTField;
    @FXML Label correoErrorLabel;
    @FXML TextField correoTField;
    @FXML Label userErrorLabel;
    @FXML TextField usuarioTField;
    @FXML Label passErrorLabel;
    @FXML TextField passTField;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void aceptarRegistro(ActionEvent event) throws IOException{
        int cont = 0;
        if(nombresTField.getText().length() < 2){
            nombreErrorLabel.setText("Mín. 2 caracteres");
        } else{
            cont++;
            nombreErrorLabel.setText("");
        }
        if(apellidosTField.getText().length() < 2){
            apellidosErrorLabel.setText("Mín. 2 caracteres");
        } else{
            cont++;
            apellidosErrorLabel.setText("");
        }
        if(!validarCorreo(correoTField.getText())){
            correoErrorLabel.setText("Ingresar correo válido");
        } else if(mailRegistrado(correoTField.getText())){
            correoErrorLabel.setText("Correo en uso, elija otro");
        } else{
            cont++;
            correoErrorLabel.setText("");
        }
        if(usuarioTField.getText().length() < 4){
            userErrorLabel.setText("Usuario inválido (Mín. 4 caracteres)");
        } else if(usuarioRegistrado(usuarioTField.getText())){
            userErrorLabel.setText("Usuario en uso, elija otro");
        } else{
            cont++;
            userErrorLabel.setText("");
        }
        if(passTField.getText().length() < 4){
            passErrorLabel.setText("Mín. 4 caracteres");
        } else{
            cont++;
            passErrorLabel.setText("");
        }
        if(cont == 5){ // se cumplen todas las condiciones
            crearUsuario();
            abrirVentana(event);
        }
        
    }
    
    @FXML
    private void cancelarRegistro(ActionEvent event) throws IOException{
        abrirVentana(event);
    }

    private void crearUsuario(){
        Usuario usuario = new Usuario(
                usuarioTField.getText(),
                passTField.getText(),
                nombresTField.getText(),
                apellidosTField.getText(),
                correoTField.getText()
        );
        Registro.getUsuariosRegistro().add(usuario);
        Registro.guardarUsuarios();
    }
    private boolean validarCorreo(String correo){
        if(correo.contains("@"))
            return true;
        return false;
    }
    private boolean usuarioRegistrado(String user){
        for(Usuario u: Registro.getUsuariosRegistro()){
            if(u.getUsuario().equals(user))
                return true;
        }
        return false;
    }
    
    private boolean mailRegistrado(String correo){
        for(Usuario u: Registro.getUsuariosRegistro()){
            if(u.getCorreo().equals(correo))
                return true;
        }
        return false;
    }
    
    private void abrirVentana(ActionEvent event) throws IOException{
        Parent singinVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Login.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(singinVentana));
        stage.setTitle("Iniciar Sesión");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
}
