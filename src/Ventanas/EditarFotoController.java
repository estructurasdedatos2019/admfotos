/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Foto;
import Funcionamiento.Usuario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import proyectodraft.Registro;

/**
 * FXML Controller class of EditarFoto, contains metods of buttons, labels...
 * @author Kevin
 */
public class EditarFotoController implements Initializable {
    FotoController fotoControllerEnEditar;
    Foto fotoActual;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfLugar;
    @FXML
    private TextField tfDescripcion;
    @FXML
    private TextArea taPersonas;
    @FXML
    private TextArea taEtiquetas;
    @FXML
    private Button btnAceptar;
    @FXML
    private Button btnRegresar;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    /**
     * Metodo que permite añadir elementos a la ventana de Editar, y se le manda una foto a esta clase
     * @param stageFoto
     * @param foto 
     */
    @FXML
    public void recibeParametros(FotoController stageFoto, Foto foto){
        tfNombre.setText(foto.getNombre());
        tfLugar.setText(foto.getLugar());
        tfDescripcion.setText(foto.getDescripcion());
        fotoActual = foto;
        fotoControllerEnEditar = stageFoto;
        
    }
    /**
     * Evento del boton Aceptar, el cual permite añadir nuevos elementos a la foto, regresamos a Foto
     * @param event 
     */
    public void accionAceptar(ActionEvent event){
        try{
            fotoActual.setDescripcion(tfDescripcion.getText());
            fotoActual.getEtiquetas().add(taEtiquetas.getText());
            fotoActual.getPersonasEnFoto().add(taPersonas.getText());
            fotoActual.setNombre(tfNombre.getText());
            fotoActual.setLugar(tfLugar.getText());
        fotoControllerEnEditar.recibeEditar(fotoControllerEnEditar, fotoActual);
        Stage stage = (Stage) btnAceptar.getScene().getWindow();
        stage.close();
        }catch(Exception e){
        Stage stage = (Stage) btnAceptar.getScene().getWindow();
        stage.close();
        }
    }
    /**
     * Evento del  boton  Regresar, regresamos a  la ventana de Foto cerrando la actual, sin cambios.
     * @param event 
     */
    public void accionRegresar(ActionEvent event){
        fotoControllerEnEditar.recibeEditar(fotoControllerEnEditar, fotoActual);
        Stage stage = (Stage) btnRegresar.getScene().getWindow();
        stage.close();
    }
    
}
