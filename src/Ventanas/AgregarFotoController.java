/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Foto;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Karla
 */
public class AgregarFotoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML private Label label;
    @FXML private AnchorPane anchorid;
    
    @FXML private TextField nombreTextField;
    @FXML private TextField descripcionTextField;
    @FXML private TextField etiquetasTextField;
    @FXML private TextField personasTextField;
    @FXML private TextField lugarTextField;
    @FXML private TextField textfield;
    
    @FXML private Label errorNombre;
    @FXML private Label errorLugar;
    @FXML private Label errorDescripcion;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    public void ventanaMain(ActionEvent event) throws IOException{
        Parent cancelVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Main.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(cancelVentana));
        stage.setTitle("My ImageKeeper - Home");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    
    @FXML
    public void buscarDirectorioImagen(){   //Abre una ventana para seleccionar el directorio del archivo
        
        final FileChooser buscarDir = new FileChooser();
        Stage stage = (Stage) anchorid.getScene().getWindow();
        File file = buscarDir.showOpenDialog(stage);
        if(file != null){
            textfield.setText(file.getAbsolutePath());  //Guarda el directorio del archivo en su respectivo textfield
        }
    }
    @FXML
    private void accionAgregarFoto(ActionEvent event) throws IOException {
        String nombre = nombreTextField.getText();
        String descripcion = descripcionTextField.getText();
        String etiquetas = etiquetasTextField.getText();
        String personas = personasTextField.getText();
        String lugar = lugarTextField.getText();
        LocalDate fecha = LocalDate.now();
        String url = textfield.getText();
        int counter = 0;
        
        if(nombre.length() < 4){
            errorNombre.setText("Min. 4 caracteres");
        }else{
            counter++;
            errorNombre.setText("");
        }
        if(lugar.isEmpty()){
            errorLugar.setText("Por favor, ingrese un lugar");
        }else{
            counter++;
            errorLugar.setText("");
        }
        if(descripcion.isEmpty()){
            errorDescripcion.setText("Añada una descripción");
        }else{
            counter++;
            errorDescripcion.setText("");
        }
        if(counter == 3){
            agregarFoto(nombre, descripcion, etiquetas, personas, lugar, fecha, url);
        }
    }
    
    public void agregarFoto(String nombre, String descripcion, String etiquetas, String personas, String lugar, LocalDate fecha, String url ){
        
    }
}