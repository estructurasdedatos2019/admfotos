/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Usuario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import proyectodraft.Registro;

/**
 * FXML Controller class
 *
 * @author Karla
 */
public class LoginController implements Initializable {
    
    @FXML TextField usuarioTField;
    @FXML TextField passField;
    @FXML Label spaceLabel;
    private static Usuario user;
    private static MainController main;
    
    private FadeTransition fadeIn = new FadeTransition(Duration.millis(3000));
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //TODO
    } 

    @FXML
    private void accionRegistrar(ActionEvent event) throws IOException {
        Parent singinVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Singin.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(singinVentana));
        stage.setTitle("Registrarme");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();

    }
    @FXML
    private void accionLogin(ActionEvent event) throws IOException {
        String usuario = usuarioTField.getText();
        String pass = passField.getText();
        
        switch (datosCorrectos(usuario,pass)) {
            case 0:
                Parent mainVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Main.fxml"));
                Stage stage= new Stage();
                stage.setScene(new Scene(mainVentana));
                stage.setTitle("My ImageKeeper - Home");
                stage.show();
                ((Node)(event.getSource())).getScene().getWindow().hide();
                break;
            case 1:
                spaceLabel.setText("Usuario o Contraseña incorrecta");
                desvanecer(spaceLabel);
                break;
            default:
                spaceLabel.setText("Usuario inexistente");
                desvanecer(spaceLabel);
                break;
        }
    }
    
    public int datosCorrectos(String usuario, String pass){
        for(Usuario u: Registro.getUsuariosRegistro()){
            if(u.getUsuario().equals(usuario) && u.getPassword().equals(pass)){
                LoginController.user = u;
                return 0;
            }
            else if(u.getUsuario().equals(usuario) && !u.getPassword().equals(pass))
                return 1;
        }
        return 2;
    }
    
    private void desvanecer(Label label){
        fadeIn.setNode(label);
        fadeIn.setFromValue(1.0);
        fadeIn.setToValue(0.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);
        fadeIn.playFromStart();
    }
    
    public static Usuario getUser(){
        return LoginController.user;
    }
    
    public static MainController getMain(){
        return LoginController.main;
    }
}

