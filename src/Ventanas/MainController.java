/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;


import Funcionamiento.Album;
import Funcionamiento.Foto;
import Funcionamiento.Usuario;
import TDAs.CircularDoublyLinkedList;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import static javafx.geometry.Pos.CENTER_LEFT;
import static javafx.geometry.Pos.TOP_LEFT;
import static javafx.scene.Cursor.HAND;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import proyectodraft.Registro;

/**
 * FXML Controller class
 *
 * @author Karla
 */
public class MainController implements Initializable {
    MainController main;
    private int codigo = 01;
    private String foto = "czzfs";
    @FXML
    private AnchorPane anchorPane;
    @FXML private Label nameLabel;
    @FXML private VBox listadoAlbumesVBox;
    @FXML private Label dateFolder;
    @FXML private Label folderName;
    @FXML private TextField searchTextField;
    @FXML private Button buscarButton;
    @FXML private VBox fotosVBox;
    
    static private Usuario usuario; // con los datos de este usuario se trabaja
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.usuario = LoginController.getUser();
        nameLabel.setText(ponerNombre(usuario));
        //aqui se van a crear todos los diccionarios
        actualizarListadoAlbumes();
        usuario.llenarMapNombreAlbumes();
        Registro.guardarUsuarios();
        System.out.println(usuario.getMapNombreAlbumes());
    }
    @FXML
    private void cerrarSesion(ActionEvent event) throws IOException{
        Parent singinVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Login.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(singinVentana));
        stage.setTitle("Iniciar Sesión");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    
    private String ponerNombre(Usuario usuario){
        StringBuilder s = new StringBuilder();
        s.append(usuario.getNombre());
        s.append(" ");
        s.append(usuario.getApellido());
        return s.toString();
    }
    
    public void actualizarListadoAlbumes(){
        listadoAlbumesVBox.getChildren().clear();
        HashMap<Integer,ArrayList<String>> mapListado = new HashMap<>(); // crear mapa con lista de codigos <año,nombreAlbum>
        System.out.println(MainController.getUsuario().getMapAlbumes().size());
                for(Album album: MainController.getUsuario().getMapAlbumes().values()){
                int año = album.getFecha().getYear();
                if(mapListado.containsKey(año))
                    mapListado.get(año).add(album.getNombre());
                else{
                    ArrayList<String> nombres = new ArrayList<>();
                    nombres.add(album.getNombre());
                    mapListado.put(año, nombres);
                }
            }
            for(Integer año: mapListado.keySet()){
                Label year = new Label(String.valueOf(año));
                year.setFont(new Font("Segoe UI Symbol",14));
                Separator sep = new Separator();
                sep.prefWidth(200);
                
                listadoAlbumesVBox.getChildren().add(year);
                listadoAlbumesVBox.getChildren().add(sep);

                System.out.println("entra por mapLisado");
                for(String nomAlbum: mapListado.get(año)){
                    HBox albumHBox = new HBox();
                    ImageView img = new ImageView();
                    img.fitHeightProperty().set(15);
                    img.fitWidthProperty().set(15);
                    Image image = new Image("/Images/icons8_folder_96px.png");
                    img.setImage(image);

                    Button boton = new Button(nomAlbum,img);
                    boton.setAlignment(TOP_LEFT);
                    boton.prefHeight(29);
                    boton.prefWidth(179);
                    boton.styleProperty().set("-fx-background-color: rgb(255,255,255,0);");
                    boton.mnemonicParsingProperty().set(false);
                    boton.cursorProperty().set(HAND);
                    boton.setFont(new Font("Segoe UI Symbol",13));
                    boton.setOnAction(event -> mostrarFotos(nomAlbum));
                    
                    albumHBox.setAlignment(CENTER_LEFT);
                    albumHBox.setPrefHeight(29);
                    albumHBox.setPrefWidth(200);
                    albumHBox.getChildren().add(boton);
                    listadoAlbumesVBox.getChildren().add(albumHBox);
                }
            }
    }
    
    @FXML
    private void crearAnimacion(ActionEvent event){ // TODO
    }
    
    private void mostrarFotos(String nomAlbum){
        int codigo = usuario.getMapNombreAlbumes().get(nomAlbum);
        Album album= usuario.getMapAlbumes().get(codigo);
        folderName.setText(nomAlbum);
        StringBuilder s = new StringBuilder();
        s.append(album.getFecha().toString());
        dateFolder.setText(s.toString());
        // agrega las fotos en el anchorPane
        HBox nuevoHBox = new HBox();
        nuevoHBox.setMinHeight(527);
        nuevoHBox.setMaxWidth(780);
        nuevoHBox.setMinHeight(203);
        nuevoHBox.setMinWidth(780);
        nuevoHBox.setPrefHeight(203);
        nuevoHBox.setPrefWidth(780);
        nuevoHBox.setSpacing(20);
        
        for(Foto foto: album.getFotosAL()){
            
        }
        
    }
    private void accionDobleClick(ActionEvent event) throws IOException{
        anchorPane.setOnMouseClicked((MouseEvent mouseEvent) -> {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                if(mouseEvent.getClickCount() == 2){
                    try{
                        Stage stageFoto = new Stage();
                        stageFoto.setTitle("Mostrar Foto");
                        FXMLLoader loader = new  FXMLLoader();
                        AnchorPane root = (AnchorPane)loader.load(getClass().getResource("../Ventanas/Foto.fxml").openStream());
                        //creo una instancia del controlador de EditarFoto
                        FotoController FotoInstancia = (FotoController)loader.getController();
                        FotoInstancia.recibeParametros(main, LoginController.getUser().getNombre(), foto, codigo);
                        Scene scene = new Scene (root);
                        stageFoto.setScene(scene);
                        stageFoto.alwaysOnTopProperty();
                        stageFoto.initModality(Modality.APPLICATION_MODAL);
                        stageFoto.show();
                    }catch(Exception e){
                        System.out.println("Excepcion al seleccionar la  foto");
                    }
                }
            }
        });
    }
    @FXML
    private void crearAlbum(ActionEvent event) throws IOException{
        Parent crearAlbumVentana = FXMLLoader.load(getClass().getResource("../Ventanas/CrearAlbum.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(crearAlbumVentana));
        stage.setTitle("Crear nuevo álbum");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    @FXML
    private void agregarFoto(ActionEvent event) throws IOException{
        Parent agregarFotoVentana = FXMLLoader.load(getClass().getResource("../Ventanas/AgregarFoto.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(agregarFotoVentana));
        stage.setTitle("Añadir nueva foto");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
        
    }
    
    public static Usuario getUsuario(){
        return MainController.usuario;
    }
    
    @FXML
    private void buscarPorEtiquetas(ActionEvent event){
        searchTextField.setPromptText("Buscar por Etiqueta");
        
    }
    @FXML
    private void buscarPorPersonas(ActionEvent event){
        searchTextField.setPromptText("Buscar por Persona");
    }
    @FXML
    private void Buscar(){
        
    }
}
