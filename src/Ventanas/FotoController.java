/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Foto;
import Funcionamiento.Usuario;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import proyectodraft.Registro;

/**
 *Esta clase provee el control al fxml Foto, y asi mismo contiene metodos para hacer cambios
 * @author kevin
 */
public class FotoController implements Initializable{
    MainController mainController;
    FotoController fotoController;
    
    private Foto fotoActual;
    
    private Image image;
    @FXML
    FotoController miControlador;
    @FXML
    private TextArea taComentario;
    @FXML
    private TextArea taComentarios;
    @FXML
    private ImageView ivFoto;
    @FXML
    private Label labelFoto;
    @FXML
    private Label labelLugar;
    @FXML
    private Label labelFecha;
    @FXML
    private Label labelUser;
    @FXML
    private Label labelCodAlbum;
    @FXML
    private TextArea taDescripcion;
    
    /**
     * Metodo que recibe parametros del MainController para ejecutar esta ventana, creando una instancia de Foto
     * @param instanciaMain
     * @param usuarioParametro
     * @param foto
     * @param codigo 
     */
    @FXML
    public void recibeParametros(MainController instanciaMain, String usuarioParametro, String foto, int codigo){
        Foto fotoRecibida = Foto.buscarFoto(usuarioParametro, foto, codigo);
        labelCodAlbum.setText(Integer.toString(codigo));
        labelFoto.setText(foto);
        labelLugar.setText(fotoRecibida.getLugar());
        labelFecha.setText(fotoRecibida.getFecha().toString());
        taComentarios.setText(fotoRecibida.getComentarios().toString());
        taDescripcion.setText(fotoRecibida.getDescripcion());
        fotoActual = fotoRecibida;
        image = new Image("file:" + fotoRecibida.getUrl());
        ivFoto.setImage(image);
        mainController = instanciaMain;
    }
    /**
     * metodo que permite enviar datos a la ventana actual, y modifica la LinkedList de usuario
     * @param instanciaFoto
     * @param foto 
     */
    @FXML
    public void recibeEditar(FotoController instanciaFoto, Foto foto){
        try{
        fotoActual = foto;
        labelFoto.setText(foto.getNombre());
        labelLugar.setText(foto.getLugar());
        labelFecha.setText(foto.getFecha().toString());
        taComentarios.setText(foto.getComentarios().toString());
        taDescripcion.setText(foto.getDescripcion());
        image = new Image("file:" + foto.getUrl());
        ivFoto.setImage(image);
        Foto.editarFoto(fotoActual, labelUser.getText(), Integer.parseInt(labelCodAlbum.getText()));}
        catch(Exception e){
            System.out.println("No se ha editado una  foto correctamente");
        }
    }
    /**
     * Evento del boton  editar, permite cargar la ventana EditarFoto, cargando datos previos de esta instancia
     * @param event 
     */
    @FXML
    private void accionEditar(ActionEvent event){
        try{
        Stage stageEditar = new Stage();
        FXMLLoader loader = new  FXMLLoader();
        AnchorPane root = (AnchorPane)loader.load(getClass().getResource("../Ventanas/EditarFoto.fxml").openStream());
        //creo una instancia del controlador de EditarFoto
        EditarFotoController editarFotoInstancia = (EditarFotoController)loader.getController();
        editarFotoInstancia.recibeParametros(fotoController, fotoActual);
        Scene scene = new Scene (root);
        stageEditar.setScene(scene);
        stageEditar.alwaysOnTopProperty();
        stageEditar.initModality(Modality.APPLICATION_MODAL);
        stageEditar.show();
        }catch(Exception e){
            System.out.println("Excepcion al presionar editar");
        }
    }
    /**
     * Evento del boton atrasar, permite buscar la foto anterior y modifica toda la ventana, asi como la foto actual
     * @param event 
     */
    @FXML
    public void accionAtrasar(ActionEvent event){
        try{
        Foto fotoAnterior = Foto.siguienteFoto(labelFoto.getText(), labelUser.getText(), Integer.parseInt(labelCodAlbum.getText()));
        labelFoto.setText(fotoAnterior.getNombre());
        labelLugar.setText(fotoAnterior.getLugar());
        labelFecha.setText(fotoAnterior.getFecha().toString());
        taComentarios.setText(fotoAnterior.getComentarios().toString());
        taDescripcion.setText(fotoAnterior.getDescripcion());
        fotoActual = fotoAnterior;
        image = new Image("file:" + fotoAnterior.getUrl());
        ivFoto.setImage(image);
        }catch(Exception e){
            System.out.println("ES LA ULTIMA FOTO, O NO SE ENCONTRO EL ARCHIVO");
        }
    }
    /**
     * Evento del boton adelantar, permite buscar la foto siguiente y modifica toda la ventana,  asi como la foto actual
     * @param event
     */
    @FXML
    public void accionAdelantar(ActionEvent event){
        try{
        Foto fotoSiguiente = Foto.fotoPrevia(labelFoto.getText(), labelUser.getText(), Integer.parseInt(labelCodAlbum.getText()));
        labelFoto.setText(fotoSiguiente.getNombre());
        labelLugar.setText(fotoSiguiente.getLugar());
        labelFecha.setText(fotoSiguiente.getFecha().toString());
        taComentarios.setText(fotoSiguiente.getComentarios().toString());
        taDescripcion.setText(fotoSiguiente.getDescripcion());
        fotoActual = fotoSiguiente;
        image = new Image("file:" + fotoSiguiente.getUrl());
        ivFoto.setImage(image);
        }catch(Exception e){
            System.out.println("ES LA ULTIMA  FOTO, O NO SE  ENCONTRO EL  ARCHIVO");
        }
    }
    /**
     * Evento del boton Volver, permite volver al main,  modificando el LinkedList de usuario
     * @param event
     * @throws IOException 
     */
    @FXML
    public void accionVolver(ActionEvent event) throws IOException {
//        Parent mainVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Main.fxml"));
//        Stage stage= new Stage();
//        stage.setScene(new Scene(mainVentana));
//        stage.show();
//        ((Node)(event.getSource())).getScene().getWindow().hide();
        //ver como  pongo metodo  en main y aqui para enlazarlos...
    }
    /**
     * Evento de Risa, añade un usuario a una reaccion y se suma
     * @param event
     * @throws IOException 
     */
    @FXML
    public void accionRisa(ActionEvent event) throws IOException {
        //Ver sobre el visitante de  fotos??? como se sabe que es comentarista
        Foto.ponerReaccion(Registro.getUsuariosRegistro().getFirst(), labelUser.getText(), "felicidad", Integer.parseInt(labelCodAlbum.getText()), labelFoto.getText());
    }
    /**
     * Evento de tristeza, añade un usuario a una reaccion y se suma
     * @param event
     * @throws IOException 
     */
    @FXML
    public void accionTriste(ActionEvent event) throws IOException {
         Foto.ponerReaccion(Registro.getUsuariosRegistro().getFirst(), labelUser.getText(), "tristeza", Integer.parseInt(labelCodAlbum.getText()), labelFoto.getText());
    }
    /**
     * Evento de Enojo, añade un usuario a una reaccion y se suma
     * @param event
     * @throws IOException 
     */
    @FXML
    public void accionEnojo(ActionEvent event) throws IOException {
         Foto.ponerReaccion(Registro.getUsuariosRegistro().getFirst(), labelUser.getText(), "enojo", Integer.parseInt(labelCodAlbum.getText()), labelFoto.getText());
    }
    /**
     * Evento de asombro, añade un usuario a una reaccion y se suma
     * @param event
     * @throws IOException 
     */
    @FXML
    public void accionAsombro(ActionEvent event) throws IOException {
         Foto.ponerReaccion(Registro.getUsuariosRegistro().getFirst(), labelUser.getText(), "asombro", Integer.parseInt(labelCodAlbum.getText()), labelFoto.getText());
    }
    /**
     * Evento de  boton moreInfo,  que muesta la descripcion en  un TextArea
     * @param event
     * @throws IOException 
     */
    @FXML
    public void moreInfo(ActionEvent event) throws IOException {
        for(Usuario us: Registro.getUsuariosRegistro()){
            if(labelUser.getText().equals(us.getUsuario())){
                for(Foto ft: us.getMapAlbumes().get(labelCodAlbum.getText()).getFotosAL()){
                    if(ft.getNombre().equals(labelFoto.getText())){
                        taDescripcion.setText(ft.getDescripcion());
                    }
                }
            }
        }
    }
    /**
     * Evento del boton Personas, que muestra las personas en un TextArea
     * @param event
     * @throws IOException 
     */
    @FXML
    public void mostrarPersonas(ActionEvent event) throws IOException {
        for(Usuario us: Registro.getUsuariosRegistro()){
            if(labelUser.getText().equals(us.getUsuario())){
                for(Foto ft: us.getMapAlbumes().get(labelCodAlbum.getText()).getFotosAL()){
                    if(ft.getNombre().equals(labelFoto.getText())){
                        taDescripcion.setText(ft.getPersonasEnFoto().toString());
                    }
                }
            }
        }
    }
    /**
     * Evento del  boton Etiquetas, que muestra las etiquetas en un TextArea
     * @param event
     * @throws IOException 
     */
    @FXML
    public void mostrarEtiquetas(ActionEvent event) throws IOException {
        for(Usuario us: Registro.getUsuariosRegistro()){
            if(labelUser.getText().equals(us.getUsuario())){
                for(Foto ft: us.getMapAlbumes().get(labelCodAlbum.getText()).getFotosAL()){
                    if(ft.getNombre().equals(labelFoto.getText())){
                        taDescripcion.setText(ft.getEtiquetas().toString());
                    }
                }
            }
        }
    }
    /**Evento del boton Comentar,  que recoge el texto del taComentario y lo añade a la foto
     * 
     * @param event 
     */
    @FXML
    public void accionComentar(ActionEvent event){//Ver como me doy cuenta cual user es
        if(!taComentario.getText().equals(null)){
            Foto.anadirComentario(labelUser.getText(), taComentario.getText(), Integer.parseInt(labelCodAlbum.getText()), labelFoto.getText(), labelUser.getText());
            taComentarios.setText(taComentarios.getText() + "\n"+taComentario.getText() + " \n               Escrito por: " + labelUser.getText());
        }else{
            taComentario.setText("Ingrese algun comentario");
        }   
    }
    /**
     * Metodo que nicializa el fotoController
     * @param location
     * @param resources 
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fotoController=this;
    }
}
