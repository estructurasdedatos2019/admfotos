/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Funcionamiento.Album;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Karla
 */
public class CrearAlbumController implements Initializable {

    @FXML private TextField nombreAlbumTField;
    @FXML private Label nombreErrorLabel;
    @FXML private TextField lugarAlbumTField;
    @FXML private Label lugarErrorLabel;
    @FXML private TextArea descripcionTextArea;
    
    @FXML
    private void aceptarCrearAlbum(ActionEvent event) throws IOException{
        int cont = 0;
        if(nombreAlbumExiste(nombreAlbumTField.getText()) || nombreAlbumTField.getText().equals("")){
            nombreErrorLabel.setText("Nombre inválido, elija otro.");
        }else{
            nombreErrorLabel.setText("");
            cont++;
        }
        if(lugarAlbumTField.getText().equals("")){
            lugarErrorLabel.setText("Campo obligatorio");
        }else{
            lugarErrorLabel.setText("");
            cont++;
        }
        if(cont == 2){
            crearAlbum();
            Parent mainVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Main.fxml"));
            Stage stage= new Stage();
            stage.setScene(new Scene(mainVentana));
            stage.setTitle("My ImageKeeper - Home");
            stage.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        }
    }
    
    @FXML
    private void cancelarCrearAlbum(ActionEvent event) throws IOException{
        ((Node)(event.getSource())).getScene().getWindow().hide();
        Parent mainVentana = FXMLLoader.load(getClass().getResource("../Ventanas/Main.fxml"));
        Stage stage= new Stage();
        stage.setScene(new Scene(mainVentana));
        stage.setTitle("My ImageKeeper - Home");
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
    
    private void crearAlbum(){
        Album album = new Album(
                nombreAlbumTField.getText(),
                lugarAlbumTField.getText(),
                descripcionTextArea.getText()
        );
        MainController.getUsuario().agregarAlbum(album); // agrega album al diccionario con su codigo
        System.out.println(MainController.getUsuario().getMapAlbumes().size());
    }
    
    private boolean nombreAlbumExiste(String nombre){
        if(MainController.getUsuario().getMapAlbumes().isEmpty())
            return false;
        for(Album album: MainController.getUsuario().getMapAlbumes().values()){
            if(album.getNombre().equals(nombre))
                return true;
        }
        return false;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
