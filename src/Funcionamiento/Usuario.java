/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionamiento;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Objects;
import java.util.PriorityQueue;
/**
 *
 * @author CltControl
 */
public class Usuario implements Serializable{
    //private String urlFoto;  DEJAR PARA DESPUES
    private String usuario, password,nombre,apellido, correo;
    private PriorityQueue<Foto> galeriaReciente; // cola, prioridad por fecha PENDIENTE
    //private LinkedList<Album> albumes; // Album debe implementar Comparable para ordenarlo segun fecha de edicion de album
    
    private HashMap<Integer,Album> mapAlbumes; // <codigo, Album>
    private HashMap<String,Integer> mapNombreAlbumes;
    private HashMap<Integer,Foto> mapFotos; // <codigo,Foto>
    
    private int contAlbumes, contFotos; // estos contadores generaran el codigo unico para cada album/foto
    
    private boolean reaccion;
    
    public Usuario(String usuario, String password, String nombre, String apellido, String correo){
        //this.urlFoto = urlFoto;
        this.usuario = usuario;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.galeriaReciente = new PriorityQueue<>();
        this.mapAlbumes = new HashMap<>();
        this.mapFotos = new HashMap<>();
        this.mapNombreAlbumes = new HashMap<>();
        this.contAlbumes = 0; this.contFotos = 0;
    }
    
    public void llenarMapNombreAlbumes(){
        for(Integer i: mapAlbumes.keySet()){
            mapNombreAlbumes.put(mapAlbumes.get(i).getNombre(), i);
           System.out.println("entra");
        }
        
    }
    
    public Usuario(String usuario, String password){
        this.usuario = usuario;
        this.password = password;
    }
    public PriorityQueue<Foto> getGaleriaReciente(){
        return this.galeriaReciente;
    }
    public HashMap<Integer,Album> getMapAlbumes(){
        return this.mapAlbumes;
    }
    public boolean reaccionarFoto(){
        return reaccion;
    }
    
//    public void agregarAlbum(String album){
//        Calendar fecha = GregorianCalendar.getInstance();
//        Album alb = new Album(album, fecha, "ubicacion");
//        albumes.add(alb);
//    }
    
    public void agregarAlbum(Album album){
        mapAlbumes.put(++contAlbumes, album);
    }
    
    public void agregarFoto(Foto foto){ 
        mapFotos.put(++contFotos, foto);
    }
    
    // un usuario solo puede reaccionar una sola vez a una foto!!!!
    
   

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public int getContAlbumes(){
        return this.contAlbumes;
    }
    
    public int getContFotos(){
        return this.contFotos;
    }

    public HashMap<String, Integer> getMapNombreAlbumes() {
        return mapNombreAlbumes;
    }

    public void setMapNombreAlbumes(HashMap<String, Integer> mapNombreAlbumes) {
        this.mapNombreAlbumes = mapNombreAlbumes;
    }

    public HashMap<Integer, Foto> getMapFotos() {
        return mapFotos;
    }

    public void setMapFotos(HashMap<Integer, Foto> mapFotos) {
        this.mapFotos = mapFotos;
    }

    public boolean isReaccion() {
        return reaccion;
    }

    public void setReaccion(boolean reaccion) {
        this.reaccion = reaccion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }
    
    
}
