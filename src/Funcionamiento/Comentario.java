/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionamiento;

import java.io.Serializable;

/**
 *
 * @author kevin
 */
public class Comentario implements Serializable{
    private String comentario;
    private String comentarista;

    public Comentario(String comentario,  String comentarista) {
        this.comentario = comentario;
        this.comentarista = comentarista;
    }

    public String getComentarista() {
        return comentarista;
    }

    public void setComentarista(String comentarista) {
        this.comentarista = comentarista;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    
    
}
