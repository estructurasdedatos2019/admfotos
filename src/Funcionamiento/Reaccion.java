/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionamiento;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author CltControl
 */
public class Reaccion implements Serializable{
    private String nombre;
    private ArrayList<Usuario> personasR;
    private int cantidad;
    
    public Reaccion(String nombre){
        this.nombre = nombre;
        this.personasR = new ArrayList<>();
        this.cantidad = 0;
    }
    
    public ArrayList<Usuario> getPersonasR(){
        return this.personasR;
    }
   
    public void aumentarCantidad(){
        this.cantidad++;
    }
    public void disminuirCantidad(){
        this.cantidad--;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
}
