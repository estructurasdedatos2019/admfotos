/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionamiento;

import TDAs.DoublyNode;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import proyectodraft.Registro;

/**
 *Clase de foto, para crear el objeto
 * @author CltControl
 */
public class Foto implements Serializable{
    
    private String nombre, descripcion;
    private ArrayList<String> etiquetas;
    private ArrayList<Comentario> comentarios;
    private ArrayList<String> personasEnFoto;
    private ArrayList<Reaccion> listaReactions; 
    private String lugar;
    private LocalDate fecha;
    private String url;
    
    /**
     * Constructor de foto, que recibe todos los parametros necesarios para la creacion
     * @param nombre
     * @param descripcion
     * @param etiquetas
     * @param personasEnFoto
     * @param lugar
     * @param fecha
     * @param url 
     */
    public Foto(String nombre,String descripcion, ArrayList<String> etiquetas, ArrayList<String> personasEnFoto, String lugar, LocalDate fecha, String url) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.etiquetas = etiquetas;
        this.personasEnFoto = personasEnFoto;
        this.comentarios = new ArrayList<>();
        this.listaReactions = new ArrayList<>();
        this.lugar = lugar;
        this.fecha = fecha;
        this.url = url;
    }
    /**
     * Constructor de Foto sin parametros, que crea las instancias
     */
    public Foto(){
        this.nombre = "null";
        this.descripcion = "null";
        this.etiquetas = new ArrayList<>();
        this.personasEnFoto = new ArrayList<>();
        this.comentarios = new ArrayList<>();
        this.listaReactions = new ArrayList<>();
        this.lugar = "";
        this.fecha = null;
        this.url = "";
    }
    /**
     * Metodo que devuelve un arraylist de reaccion, la crea
     * @return las 4 reacciones que puede tener una foto
     */
    public ArrayList<Reaccion> llenarReacciones(){
        ArrayList<Reaccion> results = new ArrayList<>();
        results.add(new Reaccion("felicidad"));
        results.add(new Reaccion("tristeza"));
        results.add(new Reaccion("enojo"));
        results.add(new Reaccion("asombro"));
        return results;
    } // VER BIEN SI VA A FOTO O A USUARIO ESTE METODO 
    /**
     * Metodo estatico que busca en la linkedlist estatica de Registro, encontrando el url de una foto
     * @param usuario
     * @param foto
     * @param codigo
     * @return el string del url de la foto
     */
    public static String buscarUrlFoto(String usuario, String foto, int codigo){
        String elRetorna = "No existe foto";
        Iterator<Usuario> usuariosRegistro = Registro.getUsuariosRegistro().iterator();
        while(usuariosRegistro.hasNext()){
            Usuario usuarioX = usuariosRegistro.next();
            if(usuarioX.getUsuario().equals(usuario) && usuarioX.getMapAlbumes().containsKey((Integer)codigo)){
                ArrayList<Foto> itFoto = usuarioX.getMapAlbumes().get((Integer)codigo).getFotosAL();
                for(Foto i: itFoto){
                    if(i.nombre.equals(foto)){
                        elRetorna = i.url;
                    }
                }
            }
        }
        return elRetorna;
    }
    /**
     * metodo estatico que devuelve una foto, tras buscarla en  el linkedlist estatico de registro
     * @param usuario
     * @param foto
     * @param codigo
     * @return una foto
     */
    public static Foto buscarFoto(String usuario, String foto, int codigo){
        Foto elRetorna = new Foto();
        Iterator<Usuario> usuariosRegistro = Registro.getUsuariosRegistro().iterator();
        while(usuariosRegistro.hasNext()){
            Usuario usuarioX = usuariosRegistro.next();
            if(usuarioX.getUsuario().equals(usuario) && usuarioX.getMapAlbumes().containsKey((Integer)codigo)){
                ArrayList<Foto> itFoto = usuarioX.getMapAlbumes().get((Integer)codigo).getFotosAL();
                for(Foto i: itFoto){
                    if(i.nombre.equals(foto)){
                        elRetorna.setNombre(i.nombre);
                        elRetorna.setDescripcion(i.descripcion);
                        elRetorna.setComentarios(i.comentarios);
                        elRetorna.setEtiquetas(i.etiquetas);
                        elRetorna.setFecha(i.fecha);
                        elRetorna.setLugar(i.lugar);
                        elRetorna.setPersonasEnFoto(i.personasEnFoto);
                        elRetorna.setListaReactions(i.listaReactions);
                        elRetorna.setUrl(i.url);
                    }
                }
            }
        }
        return elRetorna;
    }
    /**
     * metodo estatico que devuelve una foto siguiente, tras buscarla en  el linkedlist estatico de registro
     * @param fotoActual
     * @param usuario
     * @param codigo
     * @return la foto siguiente a la actual de linkedlist estatica de registro
     */
    public static Foto siguienteFoto(String fotoActual , String usuario, int codigo){
        Foto elRetorna = new Foto();
        Iterator<Usuario> usuariosRegistro = Registro.getUsuariosRegistro().iterator();
        while(usuariosRegistro.hasNext()){
            Usuario usuarioX = usuariosRegistro.next();
            if(usuarioX.getUsuario().equals(usuario) && usuarioX.getMapAlbumes().containsKey((Integer)codigo)){
                Iterator<Foto> itFotos = usuarioX.getMapAlbumes().get((Integer)codigo).getFotos().iterator();
                while(itFotos.hasNext()){
                    String fActual = itFotos.next().nombre;
                    if(fActual.equals(fotoActual)){
                        elRetorna = itFotos.next();
                    }
                }
            }
        }
        return elRetorna;
    }
    /**
     * metodo estatico que devuelve una foto anterior, tras buscarla en  el linkedlist estatico de registro
     * @param fotoActual
     * @param usuario
     * @param codigo
     * @return la foto anterior a la actual de linkedlist estatica de registro
     */
    public static Foto fotoPrevia(String fotoActual , String usuario, int codigo){
        Foto elRetorna = new Foto();
        Iterator<Usuario> usuariosRegistro = Registro.getUsuariosRegistro().iterator();
        while(usuariosRegistro.hasNext()){
            Usuario usuarioX = usuariosRegistro.next();
            if(usuarioX.getUsuario().equals(usuario) && usuarioX.getMapAlbumes().containsKey((Integer)codigo)){
                ListIterator<Foto> itFotos = (ListIterator)usuarioX.getMapAlbumes().get((Integer)codigo).getFotos().iterator();
                while(itFotos.hasPrevious()){
                    String FActual = itFotos.previous().nombre;
                    if(FActual.equals(fotoActual)){
                        elRetorna = itFotos.previous();
                    }
                }
            }
        }
        return elRetorna;
    }
    /**
     * metodo estatico que añade un comentario a una foto de dicho usuario especifico.
     * @param usuario
     * @param comentario
     * @param codigo
     * @param foto
     * @param comentarista 
     */
    public static void  anadirComentario(String usuario, String comentario, int codigo, String foto, String comentarista){
        Foto fotoAddComent = buscarFoto(usuario,foto,codigo);
        for(Usuario a : Registro.getUsuariosRegistro()){
            if(a.getUsuario().equals(usuario)){
                int i = a.getMapAlbumes().get(codigo).getFotosAL().indexOf(fotoAddComent);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).getComentarios().add(new Comentario(comentario,comentarista));
            }
        }   
        
    }
    /**
     * Metodo estatico que edita una foto de linkedlist estatica de  la clase registro,
     * @param foto
     * @param usuario
     * @param codigo 
     */
    public static void  editarFoto(Foto foto, String usuario, int codigo){
        for(Usuario a : Registro.getUsuariosRegistro()){
            if(a.getUsuario().equals(usuario)){
                int i = a.getMapAlbumes().get(codigo).getFotosAL().indexOf(foto);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setDescripcion(foto.descripcion);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setNombre(foto.nombre);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setPersonasEnFoto(foto.personasEnFoto);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setEtiquetas(foto.etiquetas);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setLugar(foto.lugar);
                a.getMapAlbumes().get(codigo).getFotosAL().get(i).setFecha(foto.fecha);
            }
        }  
    }
    /**
     * Metodo estatico que coloca una reaccion en una foto de la linkedlist estatica de registro
     * @param comentarista
     * @param user
     * @param nombreReaccion
     * @param codigo
     * @param nombreFto 
     */
    public static void ponerReaccion(Usuario comentarista, String user, String nombreReaccion, int codigo, String nombreFto){
        Foto fotoBusqueda = buscarFoto(user,nombreFto,codigo); 
        for(Usuario a : Registro.getUsuariosRegistro()){
            if(a.getUsuario().equals(user)){
                int i = a.getMapAlbumes().get(codigo).getFotosAL().indexOf(fotoBusqueda);
                ArrayList<Reaccion> reacciones = a.getMapAlbumes().get(codigo).getFotosAL().get(i).listaReactions;
                for(Reaccion reaction: reacciones){
                    if(reaction.getNombre().equals(nombreReaccion)){
                        for(Usuario us: reaction.getPersonasR()){
                            if(us.equals(comentarista)){
                            reaction.disminuirCantidad();
                            reaction.getPersonasR().remove(us);
                    }else{
                                reaction.aumentarCantidad();
                                reaction.getPersonasR().add(comentarista);
                            }
                        }
                    }
                }
            }
        }
    }
    //getters y setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<String> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(ArrayList<String> etiquetas) {
        this.etiquetas = etiquetas;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public ArrayList<String> getPersonasEnFoto() {
        return personasEnFoto;
    }

    public void setPersonasEnFoto(ArrayList<String> personasEnFoto) {
        this.personasEnFoto = personasEnFoto;
    }

    public ArrayList<Reaccion> getListaReactions() {
        return listaReactions;
    }

    public void setListaReactions(ArrayList<Reaccion> listaReactions) {
        this.listaReactions = listaReactions;
    }
    
    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
