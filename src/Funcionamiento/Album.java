/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionamiento;

import TDAs.CircularDoublyLinkedList;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author CltControl
 */
public class Album implements Serializable{
    
    private String nombre, descripcion;
    private CircularDoublyLinkedList<Foto> fotos;
    private ArrayList<Foto> fotosAL;
    private LocalDate fecha;
    private String ubicacionAlbum;

    public Album(String nombre,String ubicacionAlbum, String descripcion){
        this.nombre = nombre;
        this.ubicacionAlbum = ubicacionAlbum;
        this.descripcion = descripcion;
        this.fecha = LocalDate.now(); // se crea fecha automaticamente
        this.fotos = new CircularDoublyLinkedList<>();
        this.fotosAL = new ArrayList<>();
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Foto> getFotosAL() {
        return fotosAL;
    }

    public void setFotosAL(ArrayList<Foto> fotosAL) {
        this.fotosAL = fotosAL;
    }

    public String getUbicacionAlbum() {
        return ubicacionAlbum;
    }

    public void setUbicacionAlbum(String ubicacionAlbum) {
        this.ubicacionAlbum = ubicacionAlbum;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CircularDoublyLinkedList<Foto> getFotos() {
        return fotos;
    }

    public void setFotos(CircularDoublyLinkedList<Foto> fotos) {
        this.fotos = fotos;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    public void agregarFoto(Foto foto){
        fotos.add(foto);
        /**
         * Al crear una foto, se accede al contFotos del usuario(+1), 
         * se agrega la foto al album, y se agrega la foto al diccionario con su conFotos
         */
    }
    
//    public LinkedList<Album> crearAlbums(){
//        LinkedList<Album> albumes = new LinkedList<>();
//        albumes.add(new Album("Viaje a Francia", GregorianCalendar.getInstance(), "ubicacion1"));
//        albumes.add(new Album("Viaje a Peru", GregorianCalendar.getInstance(), "ubicacion1"));
//        albumes.add(new Album("Viaje a Grecia", GregorianCalendar.getInstance(), "ubicacion1"));
//        
//        return albumes;
//    }
//        if(!this.fotos.contains(foto)){
//            this.fotos.add(foto);
//        }
//    }
//    
//    public void  eliminarFoto(Foto foto) throws Throwable{//previa a verificacion
//        if(this.fotos.contains(foto)){
//            this.fotos.remove(foto);
//        }
//    }
    
}
