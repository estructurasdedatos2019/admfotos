/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Karla
 * @param <E>
 */
public class CircularDoublyLinkedList<E> implements Iterable<E>{
    private DoublyNode head;
    private DoublyNode nodoDoble;
    private int contador;
    public CircularDoublyLinkedList(){
        contador = 0;
    }
    public void add(E e){
        DoublyNode nodo = new DoublyNode(e);
        if(contador==0){
            nodoDoble = nodo;
            nodoDoble.setNext(nodoDoble);
            nodoDoble.setPrevious(nodoDoble);
            head = nodoDoble;
        }else{
            nodo.setPrevious(nodoDoble.getPrevious());
            nodo.setNext(nodoDoble);
            nodoDoble.getPrevious().setNext(nodo);
            nodoDoble.setPrevious(nodo);
            head = nodoDoble;
        }
        contador++;
    }
    public  void imprimirListaCircular(){
        if(contador == 0){
            System.out.println("Lista circular  vacia");
        }
        DoublyNode actual = head;
        do{
            System.out.println(actual.getData());
            actual = actual.getNext();
        }while(!actual.equals(head));
        
    }
    public void insertarNodo(int posicion, E e){
        if(posicion > contador){
            System.out.println("Posicion esta fuera  del indice");
        }
        DoublyNode nodo = new DoublyNode(e);
        DoublyNode actual = head;
        int i = 0;
        while(i<posicion){
            i++;
            actual = actual.getNext();
        }
        nodo.setPrevious(actual.getPrevious());
        nodo.setNext(actual);
        actual.getPrevious().setNext(nodo);
        actual.setPrevious(nodo);
        contador++;
    }
    public void eliminarNodo(int indice){
        if(indice > contador){
            System.out.println("Posicion esta fuera  del indice");
        }
        DoublyNode actual = head;
        int i = 0;
        while(i<indice){
            i++;
            actual = actual.getNext();
        }
        actual.getPrevious().setNext(actual.getNext());
        actual.getNext().setPrevious(actual.getPrevious());
        contador--;
    }
    
    public E get(int index){
        int currIndex = 0;
        DoublyNode<E> currNode = this.head;
        E temp = null;      
        if(index == 0){        
            temp = currNode.getData();
        }
        else if(index > 0){         
            while(currIndex != this.contador){
                if(currIndex != index%contador){
                    currIndex++;
                    currNode = currNode.getNext();                              
                }else{                              
                    temp = currNode.getData();
                    break;                                      
                }
            }   
        }
        else if(index < 0){      
            while(currIndex != -this.contador){
                if(currIndex != index%contador){
                    currIndex--;
                    currNode = currNode.getPrevious();
                }else{              
                    temp = currNode.getData();
                    break;                                      
                }
            }           
        }
        return temp;
    }   

    public DoublyNode getHead() {
        return head;
    }

    public void setHead(DoublyNode head) {
        this.head = head;
    }

    public DoublyNode getNodoDoble() {
        return nodoDoble;
    }

    public void setNodoDoble(DoublyNode nodoDoble) {
        this.nodoDoble = nodoDoble;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    @Override
    public String toString() {
        return "CircularDoublyLinkedList{" + "head=" + head + '}';
    }
    
    public Iterator<E> iterator() {
        return new CircularDoubleIterator();
    }   

    private class CircularDoubleIterator<E> implements ListIterator<E> { // for each

        private DoublyNode<E> next,last;
        private int index = 0;

        @Override
        public E next() {
            if(!this.hasNext()){
                throw new NoSuchElementException("No existe elemento.");
            }
            else{
                next = head;
                last = next.getNext();
                next = next.getNext();
                head = next; 
                index++;
                return last.getData();


            }
        }

        @Override
        public E previous() {
            if(!hasPrevious()){
                throw new NoSuchElementException("No existe elemento.");
            }
            else{
                next = head;
                last = next.getPrevious();
                next = next.getPrevious();
                head = next; 
            index--;

            return last.getData();
            }
        }
    
        @Override 
        public boolean hasNext() {  
            return contador != 0;
        }    
        @Override
        public boolean hasPrevious() {      
            return contador!= 0;
        }

        @Override
        public int nextIndex() {
            return index;
        }

        @Override
        public int previousIndex() {
            return index-1;
        }

        @Override
        public void remove() {
        }

        @Override
        public void set(E theData) {
            if(last == null){
                throw new IllegalStateException();
            }
            else{
            }

        }

        @Override
        public void add(E theData) {
            if(contador == 0){              
            }
            else if(contador != 0){
            }

        }

    }

}
